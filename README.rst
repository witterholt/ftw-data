First Time Wizard Data
======================

This repository contains the files users can download via the First Time Wizard. The licenses in these files varies, although most are Public Domain. Each file contains the license information for that content. Content that is not in the Public Domain is here by permission of the author(s) or publisher(s) of the content.

If you would like to add your content to this repository, please contact "superfly" in the #openlp IRC channel on the Libera.chat network.
